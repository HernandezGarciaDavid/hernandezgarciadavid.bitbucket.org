var Ajedrez = (function(){
  var _mostrar_tablero = function(){
    var seccion = document.getElementById("tablero");
    var tabla   = document.createElement("table");
    var cuerpoTabla = document.createElement("tbody");
    var letras = ['a','b','c','d','e','f','g','h'];
    var indice_sup = document.createElement("tr");
    indice_sup.appendChild(document.createElement("td"));
    for(var i = 0;i < 8;i ++){
      var letra = document.createElement("td");
      letra.textContent = letras[i];
      indice_sup.appendChild(letra);
    }
    cuerpoTabla.appendChild(indice_sup);
    for (var i = 8; i > 0; i--) {
      var fila = document.createElement("tr");
      var indice_izq = document.createElement("td");
      indice_izq.textContent = i;
      fila.appendChild(indice_izq);
        for (var j = 0; j < 8; j++) {
          var elemento = document.createElement("td");
          if(i%2 == 0){
            if(j%2 == 0){
              elemento.setAttribute("class","blanco ");
            }else{
              elemento.setAttribute("class","negro ");
            }
          }else{
            if(j%2 == 0){
              elemento.setAttribute("class","negro ");
            }else{
              elemento.setAttribute("class","blanco ");
            }
          }
          elemento.setAttribute("id",letras[j]+i);
          elemento.textContent = "";
          fila.appendChild(elemento);
        }
        cuerpoTabla.appendChild(fila);
    }
    tabla.appendChild(cuerpoTabla);
    tabla.setAttribute('id', 'tabla');
    seccion.appendChild(tabla);
    var opcion = document.getElementById("opcion");
    var actualizar = document.createElement("input");
    actualizar.setAttribute("type","button");
    actualizar.setAttribute("value","Actualizar");
    actualizar.addEventListener('click',_actualizar_tablero,false);
    opcion.appendChild(actualizar);
    _descargar_archivo();
  }
  var _descargar_archivo = function () {
    var servidor = "https://hernandezgarciadavid.bitbucket.io";
    var url = servidor + "/csv/tablero.csv";
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
    if (this.readyState === 4) {
       if (this.status === 200) {
           _archivo_descargado(this.responseText);
       } else {
           _mostrar_mensaje("Error " + this.status + " " + this.statusText + " - " + this.responseURL);
       }
     }
   }
   xhr.open('GET', url, true);
   xhr.send();
  }
  var _actualizar_tablero=function () {
    _mostrar_mensaje("");
    _descargar_archivo();
  }
  var _archivo_descargado = function(cadena){
    var filas = cadena.split("\n");
    var datos = [];
    var ban=true;
    if(filas.length == 9){
      for(var i=0;i<filas.length;i++){
        var fila = filas[i].split("|");
        var f = [];
        if (fila.length == 8){
          for(var j=0;j<8;j++){
            f [j]=fila[j];
            if(fila[j]===""){
            _mostrar_mensaje("");
              _mostrar_mensaje("existe un valor vacio");
              ban=false;
            }
          }
          datos[i]=f;
        }else {
        _mostrar_mensaje("");
          _mostrar_mensaje("existen filas de tamaño no apropiado");
          ban=false;
        }
      }
    }else{
    _mostrar_mensaje("");
      _mostrar_mensaje("tamaño de filas inapropiado");
    }
    if(ban === true){
    _acomodar(datos);
    }
  }
  var _acomodar = function (cadena) {
    var letras = ['a','b','c','d','e','f','g','h'];
    var cont=1;
    for(var j=8; j>0; j--){
      for (var i = 0; i < letras.length; i++) {
        if (cadena[cont][i] === "∅") {
			document.getElementById(letras[i]+j).textContent="";
        }else{
          document.getElementById(letras[i]+j).textContent=cadena[cont][i];
        }
      }
      cont++;
    }
  }
  var _mover_pieza = function(objeto){
    _mostrar_mensaje("");
    if (typeof objeto !== undefined && objeto !== null){
      if(typeof objeto === "object"){
        var origen = objeto.de;
        var destino = objeto.a;
        if(typeof origen !== undefined && typeof destino !== undefined){
          var celda_origen = document.getElementById(origen);
          var celda_destino = document.getElementById(destino);
          if(celda_origen !== null && celda_destino !== null){
            var pieza = celda_destino.textContent;
            if (pieza === ""){
              celda_destino.textContent = celda_origen.textContent;
              celda_origen.textContent = "";
            }else{
              _mostrar_mensaje("La celda tiene una pieza");
            }
          }else {
            _mostrar_mensaje("formato erroneo las cordenadas deben mostrat columna y fila, ejemplo: a8");
          }
        }else{
          _mostrar_mensaje("formato erroneo el objeto debe de tener dos elementos: \"de\" y \"a\"");
        }
      }else{
        _mostrar_mensaje("Debes de pasar un objeto con dos elmentos \"de\"(pocision de origen) y \"a\" (pocision destino)");
      }
    }else{
      _mostrar_mensaje("No existe un parametro o el parametro es nulo");
    }
  }
  var _mostrar_mensaje = function(mensaje){
    var seccion = document.getElementById("mensaje");
    var parrafo = document.createElement("p");
    parrafo.textContent = mensaje;
    seccion.appendChild(parrafo);
  }
  return{
    "mostrarTablero": _mostrar_tablero,
    "actualizarTablero": _actualizar_tablero,
    "moverPieza": _mover_pieza
  }
})();
